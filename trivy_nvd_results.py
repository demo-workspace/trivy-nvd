import json
import logging
import sys

# as defined by NIST
minLow = 0.1
minMed = 4.0
minHigh = 7.0
minCrit = 9.0


def generate_section(jsonobj, min, max):
    count = 0
    results = jsonobj['Results']
    for r in results:
        vulnerabilities = r['Vulnerabilities']
        for v in vulnerabilities:
            vid = v.get('VulnerabilityID','')
            if vid.startswith('CVE'):
                nvdScore = 99
                try:
                    sigh = v['CVSS']['nvd']
                    nvdScore = sigh['V3Score']
                except:
                    logging.warning("No NVD score for "+vid)
                if (min <= nvdScore < max):
                    title = v.get('Title',"unknown title")
                    description = v.get('Description',"no description")
                    references = v.get('References', [])
                    reflink = 'none'
                    for r in references:
                        if "nvd.nist.gov" in r:
                            reflink = r
                    print(f'\tVulnerability ID: {vid}')
                    print(f'\tScore: {nvdScore}')
                    print(f'\tTitle: {title}')
                    print(f'\tDescription: {description}')
                    print(f'\tReference: {reflink}')
                    print("\t"+"-"*35)
                    count = count + 1
    return count

if __name__ == '__main__':

    jsondata = "{}"
    if len(sys.argv) == 1:
        jsondata = sys.stdin.read()
    else:
        with open(sys.argv[1], 'r') as f:
            jsondata = f.read()

    print(f'DATA: {jsondata}')
    jsonobj = json.loads(jsondata)
    artname = jsonobj.get('ArtifactName', "artifcat name unknown")
    print('Trivy scan results')
    print(f'Artifact scanned: {artname}\n')
    print("Critical CVEs:")
    critCount = generate_section(jsonobj, minCrit, 10)
    print("\n\nHigh CVEs:")
    highCount = generate_section(jsonobj, minHigh, minCrit)
    print("\n\nMedium CVEs:")
    medCount = generate_section(jsonobj, minMed, minHigh)
    print("\n\nHigh CVEs:")
    lowCount =generate_section(jsonobj, minLow, minMed)
    print("\n\nSummary:")
    print("\tCritical CVEs:".ljust(16)+str(critCount).rjust(5))
    print("\tHigh CVEs:".ljust(16)+str(highCount).rjust(5))
    print("\tMedium CVEs:".ljust(16)+str(medCount).rjust(5))
    print("\tLow CVEs:".ljust(16)+str(lowCount).rjust(5))
